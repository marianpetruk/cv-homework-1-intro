import numpy as np
import cv2

print("CV2 version:", cv2.__version__)

img = cv2.imread("data/img_0_crop_rotated.jpg", cv2.COLOR_BGR2RGB)
img_resized = cv2.resize(img, (int(img.shape[1] * 0.3), int(img.shape[0] * 0.3)))

gray = cv2.cvtColor(img_resized, cv2.COLOR_BGR2GRAY)
cv2.imshow("img_resized initial", gray)

gray = cv2.bitwise_not(gray)

cv2.imshow("img_resized gray inverted", gray)

(thresh, blackAndWhiteImage) = cv2.threshold(gray, 0, 255, cv2.THRESH_BINARY + cv2.THRESH_OTSU)
cv2.imshow("img_resized threshold (otsu)", blackAndWhiteImage)

coords = np.column_stack(np.where(blackAndWhiteImage > 0))
angle = cv2.minAreaRect(coords)[-1]

if angle < -45:
    angle = -(90 + angle)
else:
    angle = -angle

(h, w) = img_resized.shape[:2]
center = (w // 2, h // 2)
M = cv2.getRotationMatrix2D(center, angle, 1.0)
rotated = cv2.warpAffine(img_resized, M, (w, h), flags=cv2.INTER_CUBIC, borderMode=cv2.BORDER_REPLICATE)

cv2.putText(rotated, "Angle: {:.2f} degrees".format(angle), (10, 30), cv2.FONT_HERSHEY_SIMPLEX, 0.7, (0, 0, 255), 2)

# show the output image
print("[INFO] angle: {:.3f}".format(angle))
cv2.imshow("Input", img_resized)
cv2.imshow("Rotated", rotated)

cv2.waitKey(0)
cv2.destroyAllWindows()
