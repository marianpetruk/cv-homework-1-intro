import numpy as np
import cv2


print("CV2 version:", cv2.__version__)

img = cv2.imread("data/00539986.tif", cv2.COLOR_BGR2RGB)
# img = cv2.imread("data/00539987.tif", cv2.COLOR_BGR2RGB)
# img = cv2.imread("data/00539992.tif", cv2.COLOR_BGR2RGB)
img_resized = cv2.resize(img, (int(img.shape[1] * 0.3), int(img.shape[0] * 0.3)))
gray = cv2.cvtColor(img_resized, cv2.COLOR_BGR2GRAY)
cv2.imshow("img_resized initial", gray)

# simple threshold
# cv2.threshold(source_image, threshold_value, maxVal, type_of_thresholding)
# types of thresholding are:
#   cv2.THRESH_BINARY
#   cv2.THRESH_BINARY_INV
#   cv2.THRESH_TRUNC
#   cv2.THRESH_TOZERO
#   cv2.THRESH_TOZERO_INV

# (thresh, blackAndWhiteImage) = cv2.threshold(img_resized, 127, 255, cv2.THRESH_BINARY)

# otsu thresholding - automatically calculates a threshold value from image histogram for a bimodal image.
(thresh, blackAndWhiteImage) = cv2.threshold(gray, 0, 255, cv2.THRESH_BINARY+cv2.THRESH_OTSU)
cv2.imshow("img_resized threshold (otsu)", blackAndWhiteImage)


# get all contours
contours, hierarchy = cv2.findContours(blackAndWhiteImage, cv2.RETR_TREE, cv2.CHAIN_APPROX_SIMPLE)
contours = np.array(contours)

# find largest contour
# contour_areas = list(map(cv2.contourArea, contours))
contour_areas = np.vectorize(cv2.contourArea)(contours)

# largest_contour_idx = contour_areas.index(max(contour_areas))
largest_contour_idx = np.argmax(contour_areas)


largest_contour = contours[largest_contour_idx].reshape((-1, 2))
# cv2.drawContours(img_resized, contours, largest_contour_idx, (0, 255, 0), 3)
# cv2.imshow("largest contour", img_resized)
# print(largest_contour)


min_y = np.min(largest_contour[:, 1], axis=0)
max_y = np.max(largest_contour[:, 1], axis=0)

min_x = np.min(largest_contour[:, 0], axis=0)
max_x = np.max(largest_contour[:, 0], axis=0)

margin = 20

out = img_resized[min_y+margin:max_y-margin, min_x+margin:max_x-margin]

cv2.imshow("out", out)

# mask = np.ones_like(blackAndWhiteImage)
# print(blackAndWhiteImage)

# black_pixels_indices = np.argwhere(blackAndWhiteImage==0)
#
# top_left = (np.min(black_pixels_indices[:, 0]), np.min(black_pixels_indices[:, 1]))
# bottom_right = (np.max(black_pixels_indices[:, 0]), np.max(black_pixels_indices[:, 1]))
#
# # blackAndWhiteImage[:, blackAndWhiteImage==0] = 255
#
# blackAndWhiteImage = blackAndWhiteImage[top_left, bottom_right]


# print(np.max(black_pixels_indices))


# print(np.max(blackAndWhiteImage[black_pixels_indices]))


# blackAndWhiteImage[black_pixels_indices] = 255


# print(blackAndWhiteImage)

# blackAndWhiteImage = cv2.bitwise_and(blackAndWhiteImage, blackAndWhiteImage, mask=mask)


# noise removal
kernel = np.ones((3,3),np.uint8)
opening = cv2.morphologyEx(thresh,cv2.MORPH_OPEN,kernel, iterations = 2)



kernel = np.ones((3, 3), np.uint8)
# erosion = cv2.erode(blackAndWhiteImage, kernel, iterations = 1)
dilation = cv2.dilate(blackAndWhiteImage, kernel, iterations = 1)



# blackAndWhiteImage = blackAndWhiteImage[y:y+cropy, x:x+cropx]


# w, h = blackAndWhiteImage.shape
# scale = 1

# cv2.imshow("img", cv2.resize(dilation, (int(h * scale), int(w * scale))))
# cv2.imshow("img1", cv2.resize(blackAndWhiteImage, (int(h * scale), int(w * scale))))

cv2.waitKey(0)
cv2.destroyAllWindows()
